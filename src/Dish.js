import { useParams } from "react-router-dom";
import { database } from "./database";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
let Dish = () => {
  let { id } = useParams();
  let getDishById = database.filter((each) => each.id == id);
  return (
    <>
      <div className="container">
        <div className="image-part">
          <img src={getDishById[0].imageUrl} />
        </div>
        <div className="desc-part">
          <div className="name-rate">
            <p>{getDishById[0].dishName}</p>
            <p>
              {getDishById[0].rating}
              <FontAwesomeIcon icon={faStar} />
            </p>
          </div>
          <div className="cuisine">
            <span className="tag">Cuisine:</span> {getDishById[0].cuisine}
          </div>
          <div className="ingredient">
            <span className="tag">Description:</span>
            {getDishById[0].mainIngredient}
          </div>
          <div className="desc">{getDishById[0].details}</div>
          <div className="price">
            <span className="tag">Price:</span> {getDishById[0].price}
          </div>
        </div>
      </div>
    </>
  );
};
export default Dish;
