import chicken from "./assets/chichen_masala.jpg";
import sphagetti from "./assets/sphagetti.jpg";
import shakshuka from "./assets/lynda-kechiche--RF3H8HJe5I-unsplash.jpg";
import beefWel from "./assets/beef_wellington.jpg";
import tiramisu from "./assets/Tiramisu.jpg";
import salad from "./assets/salad.jpg";
import pizza from "./assets/pizza.jpg";
import sushi from "./assets/sushi.jpg";
import tacos from "./assets/tacos.jpg";
import padThai from "./assets/shahsukha.jpg";
const database = [
  {
    id: 1,
    dishName: "Spaghetti Bolognese",
    cuisine: "Italian",
    mainIngredient: "Beef",
    difficulty: "Medium",
    preparationTime: "45 minutes",
    rating: 4.5,
    price: "$12.99",
    description:
      "Classic Italian dish with meat sauce served over spaghetti noodles.",
    imageUrl: sphagetti,
    details:
      "Spaghetti Bolognese is a quintessential Italian dish that has gained popularity worldwide. This hearty meal consists of al dente spaghetti noodles generously coated in a rich, savory meat sauce. The sauce, often prepared with ground beef, tomatoes, onions, garlic, and aromatic herbs, simmers to perfection, infusing the dish with deep flavors. Garnished with grated Parmesan cheese and fresh basil, Spaghetti Bolognese offers a comforting and satisfying dining experience that appeals to both young and old alike.",
  },
  {
    id: 2,
    dishName: "Chicken Tikka Masala",
    cuisine: "Indian",
    mainIngredient: "Chicken",
    difficulty: "Medium",
    preparationTime: "60 minutes",
    rating: 4.3,
    price: "$10.99",
    description:
      "Creamy and flavorful Indian dish made with grilled chicken in a spiced tomato sauce.",
    imageUrl: chicken,
    details:
      "Chicken Tikka Masala is a beloved Indian dish renowned for its creamy texture and robust flavor profile. Succulent pieces of marinated chicken are grilled to perfection and then simmered in a luscious, spiced tomato-based sauce. Infused with a medley of aromatic spices such as cumin, coriander, and garam masala, this dish captivates the palate with its harmonious blend of savory and slightly tangy notes. Served alongside fragrant rice or warm naan bread, Chicken Tikka Masala is a culinary delight that never fails to impress.",
  },
  {
    id: 3,
    dishName: "Caesar Salad",
    cuisine: "American",
    mainIngredient: "Lettuce",
    difficulty: "Easy",
    preparationTime: "15 minutes",
    rating: 4.0,
    price: "$8.50",
    description:
      "Classic salad made with romaine lettuce, croutons, parmesan cheese, and Caesar dressing.",
    imageUrl: salad,
    details:
      "Caesar Salad is a timeless classic that tantalizes the taste buds with its refreshing combination of crisp romaine lettuce, crunchy croutons, and tangy Caesar dressing. Originating from America, this iconic salad boasts a symphony of flavors and textures that delight the senses. Tossed with freshly grated Parmesan cheese and drizzled with a creamy dressing infused with garlic, anchovies, and lemon juice, Caesar Salad offers a perfect balance of richness and zest.",
  },
  {
    id: 4,
    dishName: "Sushi Rolls",
    cuisine: "Japanese",
    mainIngredient: "Fish",
    difficulty: "Hard",
    preparationTime: "90 minutes",
    rating: 4.7,
    price: "$15.99",
    description:
      "Assorted sushi rolls made with fresh fish, rice, and vegetables, served with soy sauce and wasabi.",
    imageUrl: sushi,
    details:
      "Sushi Rolls, also known as makizushi, are a culinary masterpiece originating from Japan. Crafted with the utmost precision and artistry, these delectable rolls feature a delicate balance of vinegared rice, fresh fish, crisp vegetables, and creamy avocado, all wrapped in a sheet of nori seaweed. Each bite offers a symphony of flavors and textures, from the subtle sweetness of the rice to the briny freshness of the seafood. Served with soy sauce, pickled ginger, and wasabi, Sushi Rolls provide a culinary experience that is both elegant and satisfying.",
  },
  {
    id: 5,
    dishName: "Beef Tacos",
    cuisine: "Mexican",
    mainIngredient: "Beef",
    difficulty: "Easy",
    preparationTime: "30 minutes",
    rating: 4.2,
    price: "$9.99",
    description:
      "Mexican street food favorite made with seasoned ground beef, lettuce, cheese, and salsa, wrapped in a soft corn tortilla.",
    imageUrl: tacos,
    details:
      "Beef Tacos are a beloved Mexican street food that embodies the vibrant flavors and bold spices of Mexican cuisine. Tender seasoned ground beef is nestled within soft corn tortillas and adorned with a medley of fresh toppings, including crisp lettuce, tangy cheese, zesty salsa, and creamy guacamole. Each bite bursts with savory goodness, offering a tantalizing mix of textures and tastes that dance on the palate. Whether enjoyed as a quick snack or a hearty meal, Beef Tacos are sure to satisfy cravings and ignite the senses.",
  },
  {
    id: 6,
    dishName: "Margherita Pizza",
    cuisine: "Italian",
    mainIngredient: "Cheese",
    difficulty: "Easy",
    preparationTime: "20 minutes",
    rating: 4.6,
    price: "$11.99",
    description:
      "Traditional Italian pizza topped with fresh mozzarella, tomatoes, basil, and olive oil.",
    imageUrl: pizza,
    details:
      "Margherita Pizza is a timeless Italian classic that celebrates the simplicity of fresh ingredients and traditional flavors. A crispy thin crust is adorned with a vibrant tomato sauce, creamy mozzarella cheese, fragrant basil leaves, and a drizzle of olive oil. Each bite offers a harmonious blend of tangy sweetness from the tomatoes, richness from the cheese, and freshness from the basil. With its unmistakable aroma and irresistible taste, Margherita Pizza is a true crowd-pleaser that never goes out of style.",
  },
  {
    id: 7,
    dishName: "Pad Thai",
    cuisine: "Thai",
    mainIngredient: "Noodles",
    difficulty: "Medium",
    preparationTime: "45 minutes",
    rating: 4.4,
    price: "$13.50",
    description:
      "Popular Thai stir-fried noodle dish made with rice noodles, shrimp, tofu, peanuts, and bean sprouts.",
    imageUrl: padThai,
    details:
      "Pad Thai is a beloved Thai street food dish renowned for its bold flavors and satisfying textures. Stir-fried rice noodles are combined with tender shrimp, crispy tofu, crunchy peanuts, and crisp bean sprouts, all tossed in a tangy-sweet sauce made from tamarind, fish sauce, and palm sugar. Each bite is a burst of flavor, with the perfect balance of sweet, savory, sour, and spicy notes. Garnished with fresh cilantro and a squeeze of lime, Pad Thai is a culinary masterpiece that transports diners to the bustling streets of Thailand.",
  },
  {
    id: 8,
    dishName: "Beef Wellington",
    cuisine: "British",
    mainIngredient: "Beef",
    difficulty: "Hard",
    preparationTime: "120 minutes",
    rating: 4.9,
    price: "$29.99",
    description:
      "Gourmet British dish made with tender beef fillet coated with pâté and wrapped in puff pastry.",
    imageUrl: beefWel,
    details:
      "Beef Wellington is a gourmet British dish that epitomizes elegance and sophistication. A tender beef fillet is coated with a layer of rich pâté and a savory duxelles made from finely chopped mushrooms, shallots, and herbs. The entire ensemble is encased in flaky puff pastry and baked to golden perfection. The result is a culinary masterpiece that delights the senses with its exquisite taste and luxurious texture. Whether served as a centerpiece for a special occasion or enjoyed as a decadent indulgence, Beef Wellington is sure to impress even the most discerning palate.",
  },
  {
    id: 9,
    dishName: "Shakshuka",
    cuisine: "Middle Eastern",
    mainIngredient: "Eggs",
    difficulty: "Medium",
    preparationTime: "30 minutes",
    rating: 4.1,
    price: "$10.99",
    description:
      "Middle Eastern dish of poached eggs in a spiced tomato and pepper sauce, often served with bread.",
    imageUrl: shakshuka,
    details:
      "Shakshuka is a vibrant and flavorful Middle Eastern dish that is as visually stunning as it is delicious. Poached eggs are nestled in a rich and spicy tomato and pepper sauce, infused with a blend of aromatic spices such as cumin, paprika, and chili powder. The dish is typically served piping hot, with crusty bread on the side for soaking up the flavorful sauce. With its bold flavors and hearty ingredients, Shakshuka is a beloved comfort food that is perfect for any meal of the day.",
  },
  {
    id: 10,
    dishName: "Tiramisu",
    cuisine: "Italian",
    mainIngredient: "Mascarpone",
    difficulty: "Medium",
    preparationTime: "60 minutes",
    rating: 4.8,
    price: "$7.99",
    description:
      "Classic Italian dessert made with layers of coffee-soaked ladyfingers and creamy mascarpone cheese, dusted with cocoa powder.",
    imageUrl: tiramisu,
    details:
      "Tiramisu is a decadent Italian dessert that delights the senses with its rich layers of flavors and textures. This classic treat features delicate ladyfinger biscuits soaked in coffee and layered with a velvety mascarpone cheese mixture. Each bite is a symphony of flavors, with the bold bitterness of the coffee perfectly complementing the creamy sweetness of the mascarpone. Dusting of cocoa powder adds a hint of bitterness and visual appeal to this indulgent dessert. Whether enjoyed as a finale to a lavish meal or savored on its own, Tiramisu is a timeless favorite that never disappoints.",
  },
];

export { database };
