import { useEffect, useState } from "react";

let Filter = () => {
  let array = [
    "kolkata",
    "dehli",
    "mumbai",
    "banglore",
    "chennai",
    "kotak",
    "bangkok",
  ];
  let [city, setCity] = useState(array);

  let [input, setInput] = useState("");

  useEffect(() => {
    console.log(city);
    let newcity = array.filter((each) => each.indexOf(input) > -1);
    setCity(newcity);
    console.log(newcity);
  }, [input]);
  return (
    <>
      <input
        type="text"
        value={input}
        onChange={(e) => {
          setInput(e.target.value);
        }}
      />
      <ul>
        {city.map((each, index) => (
          <li key={index}>{each}</li>
        ))}
      </ul>
    </>
  );
};
export default Filter;
