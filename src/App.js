import "./App.css";
import { Route, Routes } from "react-router-dom";
import Dish from "./Dish";
import Home from "./Home";
import Filter from "./Filter";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />}/>
        <Route path="/dish/:id" element={<Dish />}/>
        {/* <Route path="/" element={<Filter />} /> */}
      </Routes> 
    </>
  );
}

export default App;
