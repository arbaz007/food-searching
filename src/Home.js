import { useNavigate } from "react-router-dom";
import { database } from "./database";
import { useEffect, useState } from "react";

let Home = () => {
  let navigate = useNavigate();
  let handleClick = (id) => {
    navigate(`dish/${id}`);
  };
  let [dishes, setDishes] = useState (database)
  let [input, setInput] = useState("");
  useEffect(()=> {
    dishes = database.filter(each => each.dishName.toLowerCase().indexOf(input.toLowerCase()) > -1)
    setDishes(dishes)
  }, [input])
  console.log(input);
  return (
    <>
      <div className="search-bar"><input
        type="text"
        onChange={(e) => setInput(e.target.value)}
        value={input}
        placeholder="Search your food..."
      /></div>
      <div className="card-deck">
        {dishes.map((each) => (
          <div
            onClick={() => handleClick(each.id)}
            style={{ cursor: "pointer", marginBottom: "1rem" }}
            className="card"
          >
            <div className="card-image">
              <img src={each.imageUrl} alt="course_image" />
            </div>
            <div className="card-body">
              <div className="card-header">{each.dishName}</div>
              <div className="card-content">{each.description}</div>
              <footer className="card-footer">
                <div className="card-price">{each.price}</div>
                <div className="card-rate">{each.rating}</div>
              </footer>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};
export default Home;
